﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GeneralManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (GestorFirebase._gestor != null)
        {
            GestorFirebase._gestor.OnDatosUsuario.AddListener(DatosRecibidos);
        }

    }

    private void DatosRecibidos(int arg0, string arg1, UserData arg2)
    {
        if (arg0 == 0)
        {
            CargarMenuPrincipal();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void CargarLogin()
    {
        SceneManager.LoadScene("Login");
        //if (GestorFirebase._gestor != null)
        //{
        //    yield return new WaitUntil(() => GestorFirebase._gestor.logedIn = true);
        //}
    }

    public void CargarMenuPrincipal()
    {
        SceneManager.LoadScene("MenuPrincipal");
    }
}
