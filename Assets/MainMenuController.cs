﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    public TMP_Text usernameTxt;
    public TMP_Text ptsTxt;
    public GameObject PrefabRanking;
    public Transform contentRanking;
    public GameObject PanelRanking;
    List<GameObject> L_Destruir = new List<GameObject>();

    private void Start()
    {
        if (usernameTxt != null && GestorFirebase._gestor != null && GestorFirebase._user != null && ptsTxt != null)
        {
            usernameTxt.text = GestorFirebase._user.nombre;
            ptsTxt.text = GestorFirebase._user.highScore.ToString();
        }
    }

    public void GoGame()
    {
        SceneManager.LoadScene("GameScene");
    }
    public void ExitGame()
    {
        if (GestorFirebase._gestor != null)
        {
            GestorFirebase._gestor.SignOut();
        }
        Application.Quit();
    }
    public void GetRanking()
    {
        if (GestorFirebase._gestor != null)
        {
            GestorFirebase._gestor.OnGetRanking.AddListener(PintarRanking);
            StartCoroutine(GestorFirebase._gestor.ObtenerRanking());
        }
    }

    private void PintarRanking(int arg0, string arg1, TodosLosRankings arg2)
    {
        GestorFirebase._gestor.OnGetRanking.RemoveListener(PintarRanking);
        for (int i = 0; i < L_Destruir.Count; i++)
        {
            Destroy(L_Destruir[i]);
        }
        L_Destruir.Clear();
        if (arg0 == 0)
        {
            //Instanciar las posiciones del ranking y comprobar dónde estamos nosotros.
            if (PanelRanking != null && PrefabRanking != null && contentRanking != null)
            {
                PanelRanking.SetActive(true);
                for (int i = 0; i < arg2.L_Rankings.Count; i++)
                {
                    GameObject obj = Instantiate(PrefabRanking, contentRanking);
                    L_Destruir.Add(obj);
                    if (obj != null)
                    {
                        PrefabRankingController actualRank = obj.GetComponent<PrefabRankingController>();
                        if (actualRank != null)
                        {
                            if (actualRank.usernameTxt != null && ptsTxt != null && actualRank.fondo != null && actualRank.posTxt != null)
                            {
                                actualRank.usernameTxt.text = arg2.L_Rankings[i].name;
                                actualRank.ptsTxt.text = arg2.L_Rankings[i].puntos.ToString();
                                actualRank.posTxt.text = (i + 1).ToString();
                                //Soy yo
                                if(arg2.L_Rankings[i].idUsuario == GestorFirebase._auth.CurrentUser.UserId)
                                {
                                    actualRank.fondo.color = Color.green;
                                }
                                else
                                {
                                    actualRank.fondo.color = Color.blue;
                                }
                            }
                        }
                    }
                }
            }

        }
    }
}
