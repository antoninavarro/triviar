﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivarModelo : MonoBehaviour
{

    public categoriaPreguntas categoriaCarta;
    private GameObject gameController;
    public bool pulsado;


    private void Awake()
    {
        //Buscar el game controller
        gameController = GameObject.FindGameObjectWithTag("GameController");
    }

    // Update is called once per frame
    void Update()
    {
        //Logica para detectar el que el usuario esta tocando la pantalle
        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit Hit;

            //Calcular si el dedo del usuario esta tocando el modelo 3d de la carta
            if (Physics.Raycast(ray, out Hit))
            {
                if (gameController != null)
                {
                    if (!pulsado)
                    {

                      
                            //Activar el Canvas de preguntar
                            gameController.GetComponent<PreguntasController2>().ActiveCanvas();

                            //Cambiar el la categoria actual del game controller a la de la carta
                            gameController.GetComponent<PreguntasController2>().SetCategory(categoriaCarta);

                            //Cambiar preguntas
                            StartCoroutine(gameController.GetComponent<PreguntasController2>().CambiarPreguntas(0));

                            //Ponemos la booleana en true para que no se pueda volver a tocar en el objeto.
                            pulsado = true;
                        
                    }
                }
            }
        }
    }
}
