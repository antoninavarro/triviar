﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Auth;
using Firebase.Firestore;
using Firebase.Analytics;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Firebase;
using Firebase.Extensions;
using System;

public class GestorFirebase : MonoBehaviour
{
    public static GestorFirebase _gestor;
    [HideInInspector]
    public bool logedIn;
    public RegistrarUsuarioFirebase OnRegistrarUsuario;
    public LoginUsuarioFirebase OnLoginUsuario;
    public DetectadoUsuario OnDetectadoUsuario;
    public ObtenerDatosUsuarioFirebase OnDatosUsuario;
    public SetDatosUsuario OnSetDatosUsuario;
    public RetornoLogInFirebase OnRetornoLogin;
    public GetRanking OnGetRanking;
    private Firebase.Auth.FirebaseUser user;
    public GestorInicializado RetornoGestor;
    public static FirebaseApp _app;
    public static FirebaseAuth _auth;
    public static FirebaseFirestore _db;
    public bool loggedIn = false;
    public static UserData _user;
    private bool tenemosUsuario = false;
    public bool signedIn;
    public bool TenemosUsuario { get => tenemosUsuario; }
    bool inicializado;
    public object FirebaseStorage { get; private set; }

    public void Awake()
    {
        if (_gestor == null)
        {
            _gestor = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this.gameObject);
        }
        if (OnRegistrarUsuario == null)
        {
            OnRegistrarUsuario = new RegistrarUsuarioFirebase();
        }
        if (OnLoginUsuario == null)
        {
            OnLoginUsuario = new LoginUsuarioFirebase();
        }
        if (OnDatosUsuario == null)
        {
            OnDatosUsuario = new ObtenerDatosUsuarioFirebase();
        }
        if (OnSetDatosUsuario == null)
        {
            OnSetDatosUsuario = new SetDatosUsuario();
        }
        if(OnGetRanking == null)
        {
            OnGetRanking = new GetRanking();
        }
        if(OnRetornoLogin == null)
        {
            OnRetornoLogin = new RetornoLogInFirebase();
        }
        InitializeFirebase();
    }
    void Start()
    {
    }
    void InitializeFirebase()
    {
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                _app = Firebase.FirebaseApp.DefaultInstance;
                _db = FirebaseFirestore.DefaultInstance;
                _auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
                _auth.StateChanged += AuthStateChanged;
                inicializado = true;
                //AuthStateChanged(this, null);
                //SignOut();
                //StartCoroutine(PruebaSubirVideo());

                // Set a flag here to indicate whether Firebase is ready to use by your app.
                RetornoGestor.Invoke(0, "");
            }
            else
            {
                UnityEngine.Debug.LogError(System.String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
                RetornoGestor.Invoke(-1, "El dependency status no está available");
            }
        });
    }

    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        tenemosUsuario = _auth.CurrentUser != null;
        if (tenemosUsuario)
        {
            if (OnDetectadoUsuario != null)
            {
                OnDetectadoUsuario.Invoke(0, "");
            }

        }
        if (_auth.CurrentUser != user)
        {
            signedIn = user != _auth.CurrentUser && _auth.CurrentUser != null;
            if (!signedIn && user != null)
            {
                Debug.Log("Signed out " + user.UserId);
            }
            user = _auth.CurrentUser;
            if (signedIn)
            {
                StartCoroutine(ObtenerDatosUsuario(user.UserId));
            }
        }
        if (_auth.CurrentUser == null)
        {
            SceneManager.LoadScene("Login");
        }
        //_auth.SignOut();
    }

    private void DatosObtenidos(int arg0, string arg1, UserData arg2)
    {
        OnDatosUsuario.RemoveListener(DatosObtenidos);
        if (arg0 == 0)
        {
            SceneManager.LoadScene("MenuPrincipal");
        }
    }

    public IEnumerator ActualizarDatosUsuario(string nombreCampo, int contenidoCampo)
    {
        DocumentReference docRef = _db.Collection("Usuarios").Document(_auth.CurrentUser.UserId);
        var updateTask = docRef.UpdateAsync(nombreCampo, contenidoCampo);

        yield return new WaitUntil(() => updateTask.IsCompleted);

        if (updateTask.Exception != null)
        {
            Debug.LogWarning((updateTask.Exception.Flatten().InnerExceptions));
        }
        else
        {
            GestorFirebase._gestor.OnSetDatosUsuario.Invoke(0, "Datos actuializados");
            //Debug.Log($"El campo {nombreCampo} del usuario con id usuario {_auth.CurrentUser.UserId} se ha actualizado con éxito a {contenidoCampo}.");
        }
    }

    public void SignOut()
    {
        _auth.SignOut();
    }

    public IEnumerator ObtenerDatosUsuario(string Uid)
    {
        DocumentReference docRef = _db.Collection("Usuarios").Document(Uid);
        var lecturaTask = docRef.GetSnapshotAsync();

        yield return new WaitUntil(() => lecturaTask.IsCompleted);

        if (lecturaTask.Exception != null)
        {
            string error = GetErrorMessage(lecturaTask.Exception.Flatten().InnerExceptions);
            Debug.LogWarning($"Error al hacer la consulta: {error}");
            OnDatosUsuario.Invoke(-1, error, null);
        }
        else
        {
            DocumentSnapshot consulta = lecturaTask.Result;
            if (consulta.Exists)
            {
                //Return diccionario
                UserData datosUsuario = consulta.ConvertTo<UserData>();
                _user = datosUsuario;
                //Debug.Log(String.Format("Document data for {0} document: ", consulta.Id));
                //Debug.Log(String.Format("Email: {0}", datosUsuario.email));
                Debug.Log("[GestorFirebase](LeerDatosUsuario) Lectura realizada con éxito");
                OnDatosUsuario.Invoke(0, "", datosUsuario);
            }
            else
            {
                Debug.LogWarning("[GestorFirebase](LeerDatosUsuario) La consulta está vacía");
                OnDatosUsuario.Invoke(-1, "Couldn't read user data", null);

            }
        }
    }
    public IEnumerator LoginFirebase(string mail, string pass)
    {
        var loginTask = _auth.SignInWithEmailAndPasswordAsync(mail, pass);
        yield return new WaitUntil(() => loginTask.IsCompleted);
        int retCode = 0;
        string retMsg = "";
        if (loginTask.Exception != null)
        {
            retCode = -1;
            retMsg = "Wrong username or password";
        }
        else
        {
            user = loginTask.Result;
            if (user != null)
            {
                retMsg = "You have successfully logged in";
            }
            
        }
        if (OnRetornoLogin != null)
        {
            OnRetornoLogin.Invoke(retCode, retMsg);
        }
    }

    public IEnumerator ObtenerRanking()
    {
        yield return null;
        Firebase.Firestore.Query allUsers = _db.Collection("Usuarios").OrderByDescending("highScore").Limit(10);
        allUsers.GetSnapshotAsync().ContinueWithOnMainThread(task =>
        {
            TodosLosRankings todos = new TodosLosRankings();
            if (task.IsCompleted)
            {
                QuerySnapshot allUsersQuery = task.Result;
                foreach (DocumentSnapshot document in allUsersQuery.Documents)
                {
                    Dictionary<string, object> user = document.ToDictionary();
                    DatosRanking datos = new DatosRanking();
                    datos.idUsuario = document.Id;
                    foreach (KeyValuePair<string, object> pair in user)
                    {
                        if (pair.Key == "highScore")
                        {
                            object copas = pair.Value;
                            int puntosInt = int.Parse(pair.Value.ToString());
                            datos.puntos = puntosInt;
                            continue;
                        }
                        if (pair.Key == "nombre")
                        {
                            object nombre = pair.Value;
                            string nombreStr = pair.Value.ToString();
                            datos.name = nombreStr;
                            continue;
                        }
                        //Debug.Log("La clave es: " + pair.Key);
                        //Debug.Log("El valor es: " + pair.Value);
                    }
                    todos.L_Rankings.Add(datos);
                }

                OnGetRanking.Invoke(0, "Se han obtenido los usuarios para el ranking correctamente", todos);
            }
            else
            {
                OnGetRanking.Invoke(1, "No se han obtenido los datos del Ranking", null);
            }
        });
    }

    public string GetErrorMessage(System.Collections.ObjectModel.ReadOnlyCollection<System.Exception> exceptions)
    {
        Firebase.FirebaseException firebaseEx = null;
        bool firebaseExIsNull = true;
        int i = 0;
        while (firebaseExIsNull && i < exceptions.Count)
        {
            firebaseEx = exceptions[i] as Firebase.FirebaseException;
            if (firebaseEx != null)
            {
                firebaseExIsNull = false;
                var errorCode = (AuthError)firebaseEx.ErrorCode;
                return GetErrorMessage(errorCode);
            }
            i++;
        }

        return "An error has occurred";
    }
    private string GetErrorMessage(AuthError errorCode)
    {
        var message = "";
        switch (errorCode)
        {
            case AuthError.AccountExistsWithDifferentCredentials:
                //message = "Ya existe la cuenta con credenciales diferentes";
                message = "An account already exists with different credentials";
                break;
            case AuthError.MissingPassword:
                //message = "Hace falta el Password";
                message = "Missing password";
                break;
            case AuthError.WeakPassword:
                //message = "El password es debil";
                message = "Weak password";
                break;
            case AuthError.WrongPassword:
                //message = "El password es Incorrecto";
                message = "Wrong password";
                break;
            case AuthError.EmailAlreadyInUse:
                //message = "Ya existe la cuenta con ese correo electrónico";
                message = "Email already in use";
                break;
            case AuthError.InvalidEmail:
                //message = "Correo electronico invalido";
                message = "Invalid email";
                break;
            case AuthError.MissingEmail:
                //message = "Hace falta el correo electrónico";
                message = "Missing email";
                break;
            case AuthError.UserNotFound:
                message = "User not found";
                break;
            default:
                //message = "Ocurrió un error";
                message = errorCode.ToString();
                break;
        }
        return message;
    }
    public IEnumerator RegistrarUsuarioFirebase(string email, string pass, string username)
    {
        var registerTask = _auth.CreateUserWithEmailAndPasswordAsync(email, pass);

        yield return new WaitUntil(() => registerTask.IsCompleted);

        if (registerTask.Exception != null)
        {
            string error = GetErrorMessage(registerTask.Exception.Flatten().InnerExceptions);
            Debug.LogWarning($"Failed to register task with{error}");
            OnRegistrarUsuario.Invoke(-1, error);
        }
        else
        {
            yield return StartCoroutine(RegistrarUsuarioFirestore(_auth.CurrentUser.UserId, username));
            Debug.Log($"Successfully registered user {registerTask.Result.Email}");
            OnRegistrarUsuario.Invoke(0, "");
        }
    }

    public IEnumerator RegistrarUsuarioFirestore(string userId, string username)
    {

        DocumentReference docRef = _db.Collection("Usuarios").Document(userId);

        UserData usuario = new UserData();
        usuario.nombre = username;

        var nuevoUsuarioTask = docRef.SetAsync(usuario);

        yield return new WaitUntil(() => nuevoUsuarioTask.IsCompleted);

        if (nuevoUsuarioTask.Exception != null)
        {
            Debug.LogWarning($"No se ha podido crear el usuario nuevo en firestore debido a: {nuevoUsuarioTask.Exception.Message}");
        }
        else
        {
            Debug.Log($"El usuario con id usuario {userId} se ha registrado con éxito.");
            OnRegistrarUsuario.Invoke(0, "El usuario con id usuario se ha registrado con éxito.");
        }
    }
    public IEnumerator LoginUsuario(string email, string pass)
    {
        yield return null;
        _auth.SignInWithEmailAndPasswordAsync(email, pass).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                OnLoginUsuario.Invoke(0, "SignInWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                OnLoginUsuario.Invoke(0, "SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }
            if (task.IsCompleted)
            {
                Firebase.Auth.FirebaseUser newUser = task.Result;
                Debug.LogFormat("User signed in successfully: {0} ({1})",
                    newUser.DisplayName, newUser.UserId);
                logedIn = true;
                OnLoginUsuario.Invoke(0, "Login Succeed");
            }
        });
    }

    public void LogOut()
    {
        _auth.SignOut();
    }

    public void GetDatosUsuario(string UserId)
    {
        DocumentReference docRef = _db.Collection("Users").Document(UserId);

        docRef.GetSnapshotAsync().ContinueWith((task) =>
        {
            var snapshot = task.Result;
            if (snapshot.Exists)
            {
                if (_user == null)
                {
                    _user = new UserData();
                }
                _user = snapshot.ConvertTo<UserData>();
            }
            else
            {
                Debug.Log(String.Format("Document {0} does not exist!", snapshot.Id));
            }
        });
    }
}
public class RegistrarUsuarioFirebase : UnityEvent<int, string> { }
public class LoginUsuarioFirebase : UnityEvent<int, string> { }
public class ObtenerDatosUsuarioFirebase : UnityEvent<int, string, UserData> { }
[Serializable]
public class GestorInicializado : UnityEvent<int, string> { }
[Serializable]
public class DetectadoUsuario : UnityEvent<int, string> { }
[Serializable]
public class SetDatosUsuario : UnityEvent<int, string> { }
[Serializable]
public class GetRanking : UnityEvent<int, string,TodosLosRankings> { }
[Serializable]
public class RetornoLogInFirebase : UnityEvent<int, string> { }


[FirestoreData]
public class DatosRanking
{
    [FirestoreProperty]
    public string name { get; set; }
    [FirestoreProperty]
    public int puntos { get; set; }
    [FirestoreProperty]
    public string idUsuario { get; set; }
}
[FirestoreData]
public class TodosLosRankings
{
    [FirestoreProperty]
    public List<DatosRanking> L_Rankings { get; set; }

    public TodosLosRankings()
    {
        L_Rankings = new List<DatosRanking>();
    }
}