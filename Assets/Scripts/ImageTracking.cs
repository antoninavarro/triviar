﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.ARFoundation;


[RequireComponent(typeof(ARTrackedImageManager))]
public class ImageTracking : MonoBehaviour
{
    [SerializeField]

    private GameObject[] placeablePrefabs;

    private Dictionary<string, GameObject> spawnedPrefabs = new Dictionary<string, GameObject>();

    private ARTrackedImageManager trackedImageManager;


    private void Awake()
    {
        //Busca el trackeador de imagenes

        trackedImageManager = FindObjectOfType<ARTrackedImageManager>();

        //Añadir a la Dictionary los prefabs que queremos spawnear y que tenemos en la lista 
        foreach (GameObject prefab in placeablePrefabs)
        {
            //Instanciar cada objeto del listado
            GameObject newPrefab = Instantiate(prefab, Vector3.zero, Quaternion.identity);

            //Asegurarnos que el nombre del objeto es el mismo que el de la imagen que estamos traqueando
            newPrefab.name = prefab.name;

            //añadir al listado de objetos que se han instanciado 
            spawnedPrefabs.Add(prefab.name, newPrefab);

            //Desactivar el objeto para no verlo
            spawnedPrefabs[prefab.name].SetActive(false);



        }







    }

    private void OnEnable()
    {

        //Actualizar el numero de pregabs que se esta actualizando
        trackedImageManager.trackedImagesChanged += ImageChanged;


    }


    private void OnDisable()
    {

        //Actualizar el numero de pregabs que se esta actualizando
        trackedImageManager.trackedImagesChanged -= ImageChanged;
    }


    private void ImageChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {

        //ACtualizar el sitio del prefab si aparece una imagen de las trakeadas
        foreach (ARTrackedImage trackedImage in eventArgs.added)
        {

            UpdateImage(trackedImage);
        }
        //actualizar el sitio de los pregabs i la imagen se mueve
        foreach (ARTrackedImage trackedImage in eventArgs.updated)
        {

            UpdateImage(trackedImage);
        }

        //eliminar el prefab en caso de desaparecer la imagen
        foreach (ARTrackedImage trackedImage in eventArgs.removed)
        {

        
            spawnedPrefabs[trackedImage.name].SetActive(false);
        }


    }


    //actualizador del prefab, se usa para cuando se mueve o aparece una imagen de las trakeadas, de esta forma mejoramos el movimiento de los modelos segun se mueva la imagen
    private void UpdateImage(ARTrackedImage trackedImage)
    {
        //Registrar el nombre de la imagen que esta siendo traqueada  
        string name = trackedImage.referenceImage.name;

        //Obtener el lugar donde la imagen esta siendo traqueada
        Vector3 position = trackedImage.transform.position;
        //Obtener la rotación de la imagen que esta siendo traqueada
        Quaternion rotation = trackedImage.transform.rotation;

        //Buscar en la lista de prefabs de prefabs traqueables uno que sea el mismo nombre que el de la imagen traqueada
        GameObject newPrefab = spawnedPrefabs[name];




        //Revisamos entre todos los objetos que se se pueden instancias. POr si acaso multiples cosesas se han movido a la vez
        foreach (GameObject go in spawnedPrefabs.Values)
        {
            //Uno que sea el mismo nombre que del que este traqueando
            if (go.name == name)

            {


                //si no esta activo el el objeto, lo acitvamos en caso de que este siendo traqueado
                if (!newPrefab.activeSelf && trackedImage.trackingState == UnityEngine.XR.ARSubsystems.TrackingState.Tracking)
                {
                    go.SetActive(true);
                }

                //MOdificamos tanto la rotacion del objeto como la posicion siendo la misma que la del objeto real que esta siendo traqueado
                newPrefab.transform.position = position;
                newPrefab.transform.rotation = rotation;

                //En caso de desaparecer de la vista el objeto traqueado desactivar el objeto virtual
                if (trackedImage.trackingState == UnityEngine.XR.ARSubsystems.TrackingState.Limited)
                {

                    go.SetActive(false);

                }
            }


        }








    }



}
