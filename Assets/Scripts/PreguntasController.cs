﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PreguntasController : MonoBehaviour
{
    public GestorPreguntas _gestor;
    public TMP_Text enunciado;
    public GameObject PrefabRespuesta;
    public Transform contentRespuestas;
    string correctAnswer;
    List<GameObject> L_Respuestas;
    int aciertos;
    public void CambiarPreguntas(int pregunta)
    {
        _gestor.OnGetTrivial.AddListener(Change);
        StartCoroutine(_gestor.Consulta(pregunta));
    }

    private void Change(int arg0, string arg1, TrivialResponse arg2)
    {
        _gestor.OnGetTrivial.RemoveListener(Change);
        if (arg0 == 0)
        {
            if (L_Respuestas == null)
            {
                L_Respuestas = new List<GameObject>();
            }
            for (int i = 0; i < L_Respuestas.Count; i++)
            {
                Destroy(L_Respuestas[i]);
            }
            L_Respuestas.Clear();
            if (arg2 != null)
            {
                if (enunciado != null)
                {
                    int randomPregunta = UnityEngine.Random.Range(0,arg2.results.Count);
                    enunciado.text = arg2.results[randomPregunta].question;
                }
                if (PrefabRespuesta != null)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        GameObject RespuestaActual = Instantiate(PrefabRespuesta, contentRespuestas);
                        L_Respuestas.Add(RespuestaActual);
                    }
                    int random = UnityEngine.Random.Range(0, L_Respuestas.Count);
                    RespuestaController respuestaCorrecta = L_Respuestas[random].GetComponent<RespuestaController>();
                    if (respuestaCorrecta.txtRespuesta != null)
                    {
                        for (int i = 0; i < arg2.results.Count; i++)
                        {
                            respuestaCorrecta.txtRespuesta.text = arg2.results[i].correct_answer;
                            correctAnswer = arg2.results[i].correct_answer;
                            break;
                        }
                    }
                    int siguiente = 0;
                    for (int i = 0; i < L_Respuestas.Count; i++)
                    {
                        RespuestaController respuestaIncorrecta = L_Respuestas[i].GetComponent<RespuestaController>();
                        if (respuestaIncorrecta != null)
                        {
                            if (string.IsNullOrEmpty(respuestaIncorrecta.txtRespuesta.text))
                            {
                                for (int j = 0; j < arg2.results.Count; j++)
                                {
                                    respuestaIncorrecta.txtRespuesta.text = arg2.results[j].incorrect_answers[siguiente];
                                    siguiente++;
                                    break;
                                }
                            }
                        }
                        respuestaIncorrecta.gameObject.GetComponent<Button>().onClick.AddListener(() => ComprobarRespuesta(respuestaIncorrecta.txtRespuesta.text));
                    }
                }
            }
        }
        else
        {
            Debug.LogError(arg2);
        }

    }
    void ComprobarRespuesta(string respuesta)
    {
        if(string.Compare(respuesta,correctAnswer) == 0)
        {
            //Acierto
            aciertos++;
            SetCategory();
            Debug.Log("Has acertado");
        }
        else
        {
            //Error
            Debug.Log("Has fallado!");
        }
    }

    public void FinalizarPartida()
    {
        //TODO: Parar tiempo
        //Mostrar ScoreScreen
        //Almacenar puntuacion json
    }

    public void SetCategory()
    {
        
        //TODO: Restablecer tiempo.
        //Mostrar la pantalla de escaneo de tarjeta
    }
}
