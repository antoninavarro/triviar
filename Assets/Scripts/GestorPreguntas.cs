﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

[CreateAssetMenu(fileName = "Preguntas", menuName = "ScriptableObjects/ScriptablePreguntas", order = 1)]
public class GestorPreguntas : ScriptableObject
{
    public categoriaPreguntas categoria;
    public int cantidad;
    public difucultadPreguntas dificultad;
    public TrivialResponse trivial;
    public ObtenerTrivial OnGetTrivial;

    public void OnEnable()
    {
        if (OnGetTrivial == null)
        {
            OnGetTrivial = new ObtenerTrivial();
        }
    }
    public IEnumerator Consulta(int category)
    {
        category = (int)categoria;
        string consulta = "https://opentdb.com/api.php?amount=" + cantidad.ToString() + "&category=" + category.ToString() + "&difficulty=" + dificultad.ToString().ToLower() + "&type=multiple";
        using (UnityWebRequest webRequest = UnityWebRequest.Get(consulta))
        {

            yield return webRequest.SendWebRequest();

            if (webRequest.isDone)
            {
                string data = webRequest.downloadHandler.text;

                trivial = JsonUtility.FromJson<TrivialResponse>(data);
                string comillas = "&quot;";
                string comillaSimple = "&#039;";

                string pregunta = "";
                string opcion1 = "";
                string opcion2 = "";
                string opcion3 = "";
                string opcion4 = "";
                for (int i = 0; i < trivial.results.Count; i++)
                {
                    if (trivial.results[i].question.Contains(comillas))
                    {
                        trivial.results[i].question = trivial.results[i].question.Replace(comillas, "\"");
                    }
                    if (trivial.results[i].question.Contains(comillaSimple))
                    {
                        trivial.results[i].question = trivial.results[i].question.Replace(comillaSimple, "\'");
                    }
                    if (!string.IsNullOrEmpty(trivial.results[i].question))
                    {
                        pregunta = "\n" + trivial.results[i].question;
                        opcion1 = trivial.results[i].correct_answer;
                        opcion2 = trivial.results[i].incorrect_answers[0];
                        opcion3 = trivial.results[i].incorrect_answers[1];
                        opcion4 = trivial.results[i].incorrect_answers[2];
                    }
                }
                OnGetTrivial.Invoke(0, "ResultadosObtenidos", trivial);
                Debug.Log("La pregunta es: " + pregunta + "\n La opcion 1 es: " + opcion1 + "\nLa opcion 2 es: " + opcion2 + "\nLa opcion 3 es: " + opcion3 + "\nLa opcion 4 es: " + opcion4);
            }
            else
            {
                OnGetTrivial.Invoke(1, webRequest.error.ToString(),null);
            }
        }
    }
}

public enum difucultadPreguntas
{
    Easy,
    Medium,
    Hard
}

public enum categoriaPreguntas
{
    GeneralKnowledge = 9,
    Books,
    Film,
    Music,
    MusicalAndTheatres,
    TV,
    Videogames,
    BoardGames,
    ScienceAndNature,
    Computers,
    Mathematics,
    Mythology,
    Sports,
    Geography,
    History,
    Politics,
    Art,
    Celebrities,
    Animals,
    Vehicles,
    Comics,
    Gadgets,
    Manga,
    Cartoon
}

[System.Serializable]
public class TrivialResponse
{
    public int response_code;
    public List<ResultsTrivial> results;
    public TrivialResponse()
    {
        response_code = 0;
        results = new List<ResultsTrivial>();
    }
}

[System.Serializable]
public class ResultsTrivial
{
    public string category;
    public string type;
    public string difficulty;
    public string question;
    public string correct_answer;
    public List<string> incorrect_answers;

    public ResultsTrivial()
    {
        category = "";
        type = "";
        difficulty = "";
        question = "";
        correct_answer = "";
        incorrect_answers = new List<string>();
    }

}
public class ObtenerTrivial : UnityEvent<int, string, TrivialResponse> { }