﻿using Firebase.Firestore;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[FirestoreData]
public class UserData
{
    [FirestoreProperty]
    public string nombre { get; set; }
    [FirestoreProperty]
    public int highScore { get; set; }

    public UserData()
    {
        nombre = "";
        highScore = 0;
    }
}

