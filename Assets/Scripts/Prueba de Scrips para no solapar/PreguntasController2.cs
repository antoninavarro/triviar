﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PreguntasController2 : MonoBehaviour
{
    public GestorPreguntas _gestor;
    public TMP_Text enunciado;
    public GameObject PrefabRespuesta;
    public GameObject panelPause;
    public GameObject panelScoreScreen;
    public GameObject panel_Pregunta;
    public GameObject panel_Respuestas;
    int pauseCount;


    public Transform contentRespuestas;
    string correctAnswer;
    List<GameObject> L_Respuestas;
    int aciertos;
    bool contar;
    float timeRemaining = 60f;
    public GameObject canvasGame;
    public TMP_Text timeTxt;
    public TMP_Text aciertosTxt;
    public TMP_Text highScoreTxt;
    bool pause;
    public UserData _user;
    string path;
    [Header("ScoreScreen")]
    public TMP_Text scoreTxt;
    public AudioClip clip;
    public AudioSource _scoreRecord;
    private void Awake()
    {
        CargarJson();
     
        if (canvasGame != null)
        {
            canvasGame.SetActive(false);
        }
        if(AudioController._audio != null)
        {
            AudioController._audio.SetMusic(clip);
        }
    }

    private void Update()
    {
        if (contar)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
                timeTxt.text = Math.Round(timeRemaining).ToString();
            }
            else
            {
                contar = false;
                timeRemaining = 60;
                FinalizarPartida();
            }
        }
    }


    /// <summary>
    /// Si existe Json lo deserializamos y pintamos las puntuaciones guardadas.
    /// </summary>
    public void CargarJson()
    {
        path = Application.persistentDataPath + "/userdata.json";
        if (highScoreTxt != null)
        {
            if (System.IO.File.Exists(path))
            {
                if (_user == null)
                {
                    _user = new UserData();
                }
                string json = System.IO.File.ReadAllText(path);
                _user = JsonUtility.FromJson<UserData>(json);
            }
            else
            {
                highScoreTxt.text = "0";
            }
        }
        if (GestorFirebase._user != null)
        {
            _user = GestorFirebase._user;

        }
        highScoreTxt.text = _user.highScore.ToString();
    }

    public IEnumerator CambiarPreguntas(int pregunta)
    {
        if(enunciado != null)
        {
            enunciado.text = "Loading...";
        }
        yield return new WaitForSeconds(2f);
        if (L_Respuestas == null)
        {
            L_Respuestas = new List<GameObject>();
        }

        _gestor.OnGetTrivial.AddListener(Change);
        StartCoroutine(_gestor.Consulta(pregunta));
        //Restablecemos tiempo
    }

    /// <summary>
    /// Instanciamos las preguntas y las respuestas seegún lo que recibimos de la consulta web.
    /// </summary>
    /// <param name="arg0"></param>
    /// <param name="arg1"></param>
    /// <param name="arg2"></param>
    private void Change(int arg0, string arg1, TrivialResponse arg2)
    {
        contar = true;
        _gestor.OnGetTrivial.RemoveListener(Change);
        if (L_Respuestas.Count > 0)
        {
            for (int i = 0; i < L_Respuestas.Count; i++)
            {
                L_Respuestas[i].GetComponent<Button>().interactable = true;
                L_Respuestas[i].GetComponent<Button>().image.color = Color.white;
            }
        }
        if (arg0 == 0)
        {
            if (L_Respuestas == null)
            {
                L_Respuestas = new List<GameObject>();
            }
            for (int i = 0; i < L_Respuestas.Count; i++)
            {
                Destroy(L_Respuestas[i]);
            }
            L_Respuestas.Clear();
            if (arg2 != null)
            {
                if (enunciado != null)
                {
                    int randomPregunta = UnityEngine.Random.Range(0, arg2.results.Count);
                    enunciado.text = arg2.results[randomPregunta].question;
                }
                if (PrefabRespuesta != null)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        GameObject RespuestaActual = Instantiate(PrefabRespuesta, contentRespuestas);
                        L_Respuestas.Add(RespuestaActual);
                    }
                    int random = UnityEngine.Random.Range(0, L_Respuestas.Count);

                    RespuestaController respuestaCorrecta = L_Respuestas[random].GetComponent<RespuestaController>();

                    if (respuestaCorrecta.txtRespuesta != null)
                    {
                        for (int i = 0; i < arg2.results.Count; i++)
                        {
                            respuestaCorrecta.txtRespuesta.text = arg2.results[i].correct_answer;
                            correctAnswer = arg2.results[i].correct_answer;
                            break;
                        }
                    }
                    int siguiente = 0;
                    for (int i = 0; i < L_Respuestas.Count; i++)
                    {
                        RespuestaController respuestaIncorrecta = L_Respuestas[i].GetComponent<RespuestaController>();
                        if (respuestaIncorrecta != null)
                        {
                            if (string.IsNullOrEmpty(respuestaIncorrecta.txtRespuesta.text))
                            {
                                for (int j = 0; j < arg2.results.Count; j++)
                                {
                                    respuestaIncorrecta.txtRespuesta.text = arg2.results[j].incorrect_answers[siguiente];
                                    siguiente++;
                                    break;
                                }
                            }
                        }
                        respuestaIncorrecta.gameObject.GetComponent<Button>().onClick.AddListener(() => ComprobarRespuesta(respuestaIncorrecta.txtRespuesta.text, respuestaIncorrecta.gameObject.GetComponent<Button>()));
                    }
                }
            }
        }
        else
        {
            Debug.LogError(arg2);
        }

    }

    /// <summary>
    /// Comprobamos si la respuesta es correcta o no.
    /// </summary>
    /// <param name="respuesta"></param>
    /// <param name="btn"></param>
    void ComprobarRespuesta(string respuesta, Button btn)
    {
        if (L_Respuestas.Count > 0)
        {
            for (int i = 0; i < L_Respuestas.Count; i++)
            {
                L_Respuestas[i].GetComponent<Button>().interactable = false;
            }
        }
        contar = false;
        if (string.Compare(respuesta, correctAnswer) == 0)
        {
            //Acierto
            aciertos++;
            //Paramos el tiempo
            btn.image.color = Color.green;
            Debug.Log("Has acertado");
        }
        else
        {
            //Error
            btn.image.color = Color.red;
            Debug.Log("Has fallado!");
        }
        if (aciertosTxt != null)
        {
            aciertosTxt.text = aciertos.ToString();
        }
        StartCoroutine(CambiarPreguntas(0));
    }

    public void FinalizarPartida()
    {
        
        contar = false;
        GuardarJson();
      
    }

    /// <summary>
    /// Almacenamos los datos en un Json.
    /// </summary>
    public void GuardarJson()
    {
        if (_user == null)
        {
            _user = new UserData();
        }
        if (aciertos > _user.highScore)
        {
            _user.highScore = aciertos;
            if(_scoreRecord != null)
            {
                _scoreRecord.Play();
            }
            if (scoreTxt != null)
            {
                scoreTxt.text = _user.highScore.ToString();
            }
            string json = JsonUtility.ToJson(_user);
            System.IO.File.WriteAllText(path, json);

            if (GestorFirebase._gestor != null)
            {
                GestorFirebase._gestor.OnSetDatosUsuario.AddListener(DatosSubidos);
                StartCoroutine(GestorFirebase._gestor.ActualizarDatosUsuario("highScore", _user.highScore));
            }
        }
        else
        {
            scoreTxt.text = aciertos.ToString();
        }
        if (panelScoreScreen != null)
        {
            panelScoreScreen.SetActive(true);
        }
    }

    private void DatosSubidos(int arg0, string arg1)
    {
        GestorFirebase._gestor.OnSetDatosUsuario.RemoveListener(DatosSubidos);
        if(arg0 == 0)
        {
            Debug.Log("Datos actualizados");
        }
    }

    public void PauseGame()
    {

        if (panelPause != null && panel_Pregunta != null && panel_Respuestas)
        {
            pauseCount++;
            //Si pausas el juego más de 3 veces se te penalizará con 5 segundos menos
            if (pauseCount > 3)
            {
                timeRemaining -= 5f;
            }
            if (!pause)
            {
                pause = true;
                Time.timeScale = 0;
                panelPause.SetActive(true);
                panel_Pregunta.SetActive(false);
                panel_Respuestas.SetActive(false);
            }
            else
            {
                pause = false;
                Time.timeScale = 1;
                panelPause.SetActive(false);
                panel_Pregunta.SetActive(true);
                panel_Respuestas.SetActive(true);
            }
        }
    }

    public void SetCategory(categoriaPreguntas categoria)
    {

        //TODO: Restablecer tiempo.
        //Cambiar la categoria del canvas de las preguntas
        _gestor.categoria = categoria;
    }

    public void ActiveCanvas()
    {
        //Activar el Canvas
        canvasGame.SetActive(true);   
        StartCoroutine(CambiarPreguntas(0));
    }

    public void HiddeCanvas()
    {
        //Desactivar el Canvas
        canvasGame.SetActive(false);

    }
    public void ExitGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MenuPrincipal");
    }
}
