﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public static AudioController _audio;
    public AudioSource music;
    public void Awake()
    {
        if(_audio == this)
        {
            Destroy(this);
        }
        else
        {
            _audio = this;
            DontDestroyOnLoad(this);
        }
    }

    public void SetMusic(AudioClip audio)
    {
        if(music == null)
        {
            music = new AudioSource();
            music = this.gameObject.GetComponent<AudioSource>();
        }

        music.clip = audio;
        music.Play();
    }
}
