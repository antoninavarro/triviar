﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoginController : MonoBehaviour
{

    public TMP_InputField InputEmailRegistro;
    public TMP_InputField InputPassRegistro;
    public TMP_InputField InputUsernameRegistro;
    public TMP_InputField InputEmailLogin;
    public TMP_InputField InputPassLogin;
    public TMP_Text erroresTxt;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void RegistrarUsuario()
    {
        if (GestorFirebase._gestor != null)
        {
            if (InputEmailRegistro != null && InputPassRegistro != null && InputUsernameRegistro != null)
            {

                //TODO:Pantalla de carga.
                GestorFirebase._gestor.OnRegistrarUsuario.AddListener(RegistroCompleto);
                StartCoroutine(GestorFirebase._gestor.RegistrarUsuarioFirebase(InputEmailRegistro.text, InputPassRegistro.text, InputUsernameRegistro.text));
            }
            else
            {
                erroresTxt.gameObject.SetActive(true);
                erroresTxt.text = "Usuario o contraseña incorrectos";
            }
        }
    }

    public void LoginUsuario()
    {
        if (GestorFirebase._gestor != null)
        {
            if (InputEmailLogin != null && InputPassLogin != null)
            {

                //TODO:Pantalla de carga.
                GestorFirebase._gestor.OnRetornoLogin.AddListener(DatosObtenidos);
                StartCoroutine(GestorFirebase._gestor.LoginFirebase(InputEmailLogin.text, InputPassLogin.text));
            }
            else
            {
                if (erroresTxt != null)
                {
                    erroresTxt.gameObject.SetActive(true);
                    erroresTxt.text = "Usuario o contraseña incorrectos";
                }
            }
        }
    }

    private void DatosObtenidos(int arg0, string arg1)
    {
        GestorFirebase._gestor.OnRetornoLogin.RemoveListener(DatosObtenidos);

        if (arg0 == 0)
        {
            GestorFirebase._gestor.OnDatosUsuario.AddListener(OnDatosObtenidos);
            StartCoroutine(GestorFirebase._gestor.ObtenerDatosUsuario(GestorFirebase._auth.CurrentUser.UserId));
        }
        else
        {
            if (erroresTxt != null)
            {
                erroresTxt.gameObject.SetActive(true);
                erroresTxt.text = arg1;
            }
        }
    }

    private void OnDatosObtenidos(int arg0, string arg1, UserData arg2)
    {
        GestorFirebase._gestor.OnDatosUsuario.RemoveListener(OnDatosObtenidos);
        if (arg0 == 0)
        {
            GoMenu();
        }
        else
        {
            if (erroresTxt != null)
            {
                erroresTxt.gameObject.SetActive(true);
                erroresTxt.text = arg1;
            }
        }
    }

    private void RegistroCompleto(int arg0, string arg1)
    {
        GestorFirebase._gestor.OnRegistrarUsuario.RemoveListener(RegistroCompleto);

        if (arg0 == 0)
        {
            Debug.Log(arg1);

            //TODO: Registrar al usuario en Firestore con el nombre de usuario.
            GoMenu();
        }
        else
        {
            if (erroresTxt != null)
            {
                erroresTxt.gameObject.SetActive(true);
                erroresTxt.text = arg1;
            }
        }
    }

    private void GoMenu()
    {
        SceneManager.LoadScene("MenuPrincipal");
    }
}
